/*
 * Copyright (C) 2018 Esteban López | gnu_stallman (at) protonmail (dot) ch
 *
 * This file is part of TER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package gui;

import backend.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.imageio.ImageIO;

public class MainFrame extends JFrame implements ActionListener {
	private JLabel currentPlayerLabel;
	private JButton newGame, exit, next, help;
	private BoardMatrix boardMatrix;
	private ScoreBoard scoreBoard;
	private Game game;
	
	public MainFrame(Game game) {
		this.game = game;
		
		setTitle("TER");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		position(430, 230);

		// favicon
		try {
			setIconImage(ImageIO.read(this.getClass().getResource("/logo.png")));
		} catch (IOException e) {
			System.err.println("Error while reading favicon\n");
			e.printStackTrace();
		}
		
		boardMatrix = new BoardMatrix(game);
		currentPlayerLabel = new JLabel();
		updateCurrentPlayerLabel();
		Box boardPlayerBox = Box.createVerticalBox();
		currentPlayerLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		boardPlayerBox.add(boardMatrix);
		boardPlayerBox.add(Box.createRigidArea(new Dimension(0,4)));
		boardPlayerBox.add(currentPlayerLabel);
		
		newGame = new JButton("New Game");
		newGame.addActionListener(this);
		exit = new JButton("Exit");
		exit.addActionListener(this);
		next = new JButton("Next");
		next.addActionListener(this);
		next.setVisible(game.isAIvsAI());
		
		Box buttons = Box.createHorizontalBox();
		buttons.add(next);
		buttons.add(Box.createGlue());
		buttons.add(newGame);
		buttons.add(Box.createGlue());
		buttons.add(exit);

		help = new JButton("?");
		help.addActionListener(this);
		help.setAlignmentX(Component.CENTER_ALIGNMENT);
		help.setVisible(!game.isAIvsAI());

		scoreBoard = new ScoreBoard(game);
		Box scoreboardButtons = Box.createVerticalBox();
		scoreboardButtons.add(Box.createGlue());
		scoreboardButtons.add(scoreBoard);
		scoreboardButtons.add(Box.createGlue());
		scoreboardButtons.add(help);
		scoreboardButtons.add(Box.createGlue());
		scoreboardButtons.add(buttons);
		
		Box mainBox = Box.createHorizontalBox();
		mainBox.add(Box.createGlue());
		mainBox.add(boardPlayerBox);
		mainBox.add(Box.createGlue());
		mainBox.add(scoreboardButtons);
		mainBox.add(Box.createGlue());
		
		game.setMainFrame(this);
		
		add(mainBox);
		setVisible(true);
	}
	
	/**
	 * Sets a new player for current player label
	 */
	public void updateCurrentPlayerLabel() {
		currentPlayerLabel.setText(game.getCurrentPlayer().getName() + " [" + game.getCurrentPlayer().getPiece() + "]");
	}
	
	/**
	 * @return the scoreBoard
	 */
	public ScoreBoard getScoreBoard() {
		return scoreBoard;
	}

	/**
	 * @return the boardMatrix
	 */
	public BoardMatrix getBoardMatrix() { return boardMatrix; }

	/**
	 * Sets button Next visibility
	 * @param visible
	 */
	public void buttonNextVisible(boolean visible) {
		next.setVisible(visible);
	}

	/**
	 * Sets button ? (help) visibility
	 * @param visible
	 */
	public void buttonHelpVisible(boolean visible) {
		help.setVisible(visible);
	}

	/**
	 * Sets button ? (help) enabled or disabled
	 * @param enabled
	 */
	public void buttonHelpEnabled(boolean enabled) {
		help.setEnabled(enabled);
	}

	/**
	 * Sets the frame's size with centered position
	 * 
	 * @param sizeX width
	 * @param sizeY height
	 */
	private void position(int sizeX, int sizeY) {
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int marginX, marginY;
		
		marginX = (screen.width - sizeX) / 2;
		marginY = (screen.height - sizeY) / 2;
		setBounds(marginX, marginY, sizeX, sizeY);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource().equals(exit)) {
			System.exit(0);
		} else if (arg0.getSource().equals(newGame)) {
			game.newGame();
			scoreBoard.update(false);
			boardMatrix.updateBoard(null);
			updateCurrentPlayerLabel();
		} else if (arg0.getSource().equals(next)) {
			if (game.isAIvsAI()) {
				WinningCombination wc = game.move(null);
				boardMatrix.updateBoard(wc);
				if (wc != null)
					game.getBoard().cleanBoard();
				else
					scoreBoard.update(false);
			} else {
				game.getBoard().cleanBoard();
				boardMatrix.updateBoard(null);
				scoreBoard.update(false);
				updateCurrentPlayerLabel();
				next.setVisible(false);
				help.setEnabled(true);
			}

			// If Human vs AI and starting with AI turn, trigger his movement
			// No need to update scoreboard. Can't win in the first turn
			if (game.isHumanVsAI() && !game.isPlayer1Turn())
				boardMatrix.updateBoard(game.move(null));
		} else if (arg0.getSource().equals(help)) {
			boardMatrix.suggestPosition(
					(new AI("", game.getCurrentPlayer().getPiece(), game.getBoard())).newPiece()
			);
		}
	}
}
