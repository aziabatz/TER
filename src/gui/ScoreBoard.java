/*
 * Copyright (C) 2018 Esteban López | gnu_stallman (at) protonmail (dot) ch
 *
 * This file is part of TER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package gui;

import backend.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class ScoreBoard extends JPanel {
	private Game game;
	private JLabel round, player1, player2, draws;
	private int lastPlayer1, lastPlayer2, lastDraws;
	
	public ScoreBoard(Game game) {
		this.game = game;
		
		round = new JLabel();
		player1 = new JLabel();
		player2 = new JLabel();
		draws = new JLabel();
		round.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 17));
		player1.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 14));
		player2.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 14));
		draws.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 14));

		Box verticalBox = Box.createVerticalBox();
		verticalBox.add(round);
		verticalBox.add(Box.createVerticalStrut(10));
		verticalBox.add(player1);
		verticalBox.add(player2);
		verticalBox.add(draws);

		lastPlayer1 = game.getPlayer1().getPoints();
		lastPlayer2 = game.getPlayer2().getPoints();
		lastDraws = Player.getDrawPoints();
		
		update(false);
		add(verticalBox);
	}

    /**
     * Updates the scoreboard taking game's data
     */
	public void update(boolean underline) {
		int currentPlayer1, currentPlayer2, currentDraws;

		currentPlayer1 = game.getPlayer1().getPoints();
		currentPlayer2 = game.getPlayer2().getPoints();
		currentDraws = Player.getDrawPoints();

		// Text
		round.setText("Round: " + game.getRound());
		if (currentPlayer1 > lastPlayer1 && underline)  // Underline if punctuation changed
			player1.setText("<HTML><U>" + game.getPlayer1().getName() + "</U>: " + currentPlayer1 + "</HTML>");
		else
			player1.setText(game.getPlayer1().getName() + ": " + currentPlayer1);
		if (currentPlayer2 > lastPlayer2 && underline)
			player2.setText("<HTML><U>" + game.getPlayer2().getName() + "</U>: " + currentPlayer2 + "</HTML>");
		else
			player2.setText(game.getPlayer2().getName() + ": " + currentPlayer2);
		if (currentDraws > lastDraws && underline)
			draws.setText("<HTML><U>Draws</U>: " + currentDraws + "</HTML>");
		else
			draws.setText("Draws: " + currentDraws);

		// Colors
		if (game.getPlayer1().getPoints() > game.getPlayer2().getPoints()) {
			player1.setForeground(Color.RED);
			player2.setForeground(Color.BLACK);
		} else if (game.getPlayer2().getPoints() > game.getPlayer1().getPoints()) {
			player2.setForeground(Color.RED);
			player1.setForeground(Color.BLACK);
		} else {
			player1.setForeground(Color.BLACK);
			player2.setForeground(Color.BLACK);
		}

		// Update last punctuations
		lastPlayer1 = currentPlayer1;
		lastPlayer2 = currentPlayer2;
		lastDraws = currentDraws;
	}
}
