/*
 * Copyright (C) 2018 Esteban López | gnu_stallman (at) protonmail (dot) ch
 *
 * This file is part of TER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package gui;

import backend.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NamesDialog extends JDialog implements ActionListener {
	private JButton play;
	private JTextField player1, player2;
	private JRadioButton player1_X, player1_O, player2_X, player2_O;
	private Piece piecePlayer1, piecePlayer2;
	
	/**
	 * Class constructor with the gamemode
	 * <h1>USE ONLY WITH HUMAN_AI AND HUMAN_HUMAN</h1>
	 * 
	 * @param gameMode the gamemode
	 */
	public NamesDialog(GameMode gameMode, Player lastPlayer1, Player lastPlayer2) {
		super(new JFrame(), "Player's names", true);
		position(350, 200);
		
		// Radiobuttons
		ButtonGroup radioPlayer1, radioPlayer2;
		player1_X = new JRadioButton("X", true);
		player1_O = new JRadioButton("O", false);
		player2_X = new JRadioButton("X", false);
		player2_O = new JRadioButton("O", true);
		player1_X.addActionListener(this); 
		player1_O.addActionListener(this); 
		player2_X.addActionListener(this); 
		player2_O.addActionListener(this); 
		radioPlayer1 = new ButtonGroup();
		radioPlayer2 = new ButtonGroup();
		radioPlayer1.add(player1_X);
		radioPlayer1.add(player1_O);
		radioPlayer2.add(player2_X);
		radioPlayer2.add(player2_O);
		
		// Player 1 area
		player1 = new JTextField(15);
		player1.setMaximumSize(player1.getPreferredSize());
		if (lastPlayer1 != null && !(lastPlayer1 instanceof AI))
			player1.setText(lastPlayer1.getName());
		Box player1Box = Box.createHorizontalBox();
		player1Box.add(Box.createGlue());
		player1Box.add(new JLabel("Player 1:"));
		player1Box.add(Box.createHorizontalStrut(5));
		player1Box.add(player1);
		player1Box.add(Box.createHorizontalStrut(5));
		player1Box.add(player1_X);
		player1Box.add(player1_O);
		player1Box.add(Box.createGlue());
		
		// Player 2 area
		Box player2Box = null;
		if (gameMode == GameMode.HUMAN_HUMAN) {
			player2 = new JTextField(15);
			player2.setMaximumSize(player2.getPreferredSize());
			if (lastPlayer2 != null && !(lastPlayer2 instanceof AI))
				player2.setText(lastPlayer2.getName());
			player2Box = Box.createHorizontalBox();
			player2Box.add(Box.createGlue());
			player2Box.add(new JLabel("Player 2:"));
			player2Box.add(Box.createHorizontalStrut(5));
			player2Box.add(player2);
			player2Box.add(Box.createHorizontalStrut(5));
			player2Box.add(player2_X);
			player2Box.add(player2_O);
			player2Box.add(Box.createGlue());
		}
		
		Box center = Box.createVerticalBox();
		center.add(Box.createGlue());
		center.add(player1Box);
		if (gameMode == GameMode.HUMAN_HUMAN)
			center.add(player2Box);
		center.add(Box.createGlue());
		
		// Button area
		play = new JButton("Play");
		play.addActionListener(this);
		Box button = Box.createHorizontalBox(), south = Box.createVerticalBox();
		button.add(Box.createGlue());
		button.add(play);
		button.add(Box.createGlue());
		south.add(button);
		south.add(Box.createVerticalStrut(20));
		
		setLayout(new BorderLayout());
		add(center, BorderLayout.CENTER);
		add(south, BorderLayout.SOUTH);
		setVisible(true);
	}
	
	/**
	 * Retrieves the name of the Player 1
	 * 
	 * @return Player 1 name
	 */
	public String getPlayer1() {
		return player1.getText();
	}
	
	/**
	 * Retrieves the name of the Player 2
	 * 
	 * @return Player 2 name or null if not HUMAN_HUMAN gamemode
	 */
	public String getPlayer2() {
		if (player2 != null)
			return player2.getText();
		else 
			return null;
	}
	
	/**
	 * Retrieves the piece of the Player 1
	 * 
	 * @return the piecePlayer1
	 */
	public Piece getPiecePlayer1() {
		return piecePlayer1;
	}

	/**
	 * Retrieves the piece of the Player 2
	 * 
	 * @return the piecePlayer2
	 */
	public Piece getPiecePlayer2() {
		return piecePlayer2;
	}

	/**
	 * Sets the frame's size with centered position
	 * 
	 * @param sizeX width
	 * @param sizeY height
	 */
	private void position(int sizeX, int sizeY) {
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		int marginX, marginY;
		
		marginX = (screen.width - sizeX) / 2;
		marginY = (screen.height - sizeY) / 2;
		setBounds(marginX, marginY, sizeX, sizeY);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource().equals(play)) {
			if (player1_X.isSelected()) {
				piecePlayer1 = Piece.X;
				piecePlayer2 = Piece.O;
			} else {
				piecePlayer1 = Piece.O;
				piecePlayer2 = Piece.X;
			}
			
			dispose();	
			getOwner().dispose();
		} else if (arg0.getSource().equals(player1_X)) {
			player2_O.setSelected(true);
		} else if (arg0.getSource().equals(player1_O)) {
			player2_X.setSelected(true);
		} else if (arg0.getSource().equals(player2_X)) {
			player1_O.setSelected(true);
		} else if (arg0.getSource().equals(player2_O)) {
			player1_X.setSelected(true);
		}
	}
}
