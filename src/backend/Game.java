/*
 * Copyright (C) 2018 Esteban López | gnu_stallman (at) protonmail (dot) ch
 *
 * This file is part of TER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package backend;

import gui.*;
import javax.swing.JOptionPane;
import java.util.ArrayDeque;

public class Game {
	private Player player1, player2;
	private Board board;
	private MainFrame mainFrame;
	private int round;
	private boolean player1Turn, player1IsHuman, player2IsHuman;
	private ArrayDeque<Position> history;
	
	/**
	 * Game constructor starting dialogs
	 */
	public Game() {
		board = Board.getBoardInstance();
		history = new ArrayDeque<Position>();
		newGame();
	}
	
	/**
	 * Sets the mainframe
	 * @param mainFrame sets the MainFrame
	 */
	public void setMainFrame(MainFrame mainFrame) {
		this.mainFrame = mainFrame;
	}
	
	/**
	 * @return the player1
	 */
	public Player getPlayer1() {
		return player1;
	}

	/**
	 * @return the player2
	 */
	public Player getPlayer2() {
		return player2;
	}
	
	/**
	 * @return the player in turn
	 */
	public Player getCurrentPlayer() {
		return player1Turn ? player1:player2;
	}

	/**
	 * @return the board
	 */
	public Board getBoard() {
		return board;
	}

	/**
	 * @return the player1Turn
	 */
	public boolean isPlayer1Turn() {
		return player1Turn;
	}

	/**
	 * @return the player2IsHuman
	 */
	public boolean isPlayer2IsHuman() {
		return player2IsHuman;
	}

	/**
	 * @return true if both players are AIs
	 */
	public boolean isAIvsAI() {
		return !player1IsHuman && !player2IsHuman;
	}

	/**
	 * @return true if player1 is human and player2 is an AI
	 */
	public boolean isHumanVsAI() {
		return player1IsHuman && !player2IsHuman;
	}

	/**
	 * @return the round
	 */
	public int getRound() {
		return round;
	}

	/**
	 * @return history deque
	 */
	public ArrayDeque<Position> getHistory() {
		return history;
	}

	/**
	 * Erases the board and set punctuations to 0
	 * <p>IT KEEPS THE SAME PLAYERS</p>
	 */
	public void newGame() {
		GameMode gameMode;
		String namePlayer1 = null, namePlayer2 = null;
		NamesDialog namesDialog = null;
		boolean namesOK;

		gameMode = (new GameSelector()).getSelection();

		if (gameMode != GameMode.AI_AI) {
			do {
                namesDialog = new NamesDialog(gameMode, player1, player2);
                namePlayer1 = namesDialog.getPlayer1();
                namePlayer2 = namesDialog.getPlayer2();
                namesOK = true;

                // Names check
                if (namePlayer1.equals("") || (namePlayer2 != null && namePlayer2.equals(""))) {
                    JOptionPane.showMessageDialog(null, "Pwease tell me your name Onii-chan","Error",
                            JOptionPane.WARNING_MESSAGE);
                    namesOK = false;
                }
            } while (!namesOK);
		}

		switch (gameMode) {
			case HUMAN_AI:
				player1 = new Player(namePlayer1, namesDialog.getPiecePlayer1());
				player2 = new AI("AI", namesDialog.getPiecePlayer2(), board);
				break;
			case HUMAN_HUMAN:
				player1 = new Player(namePlayer1, namesDialog.getPiecePlayer1());
				player2 = new Player(namePlayer2, namesDialog.getPiecePlayer2());
				break;
			default:
				player1 = new AI("AI1", Piece.O, board);
				player2 = new AI("AI2", Piece.X, board);
		}

		board.cleanBoard();
		Player.setDrawPoints(0);
		player1Turn = true;
		player1IsHuman = !(player1 instanceof AI);
		player2IsHuman = !(player2 instanceof AI);
		round = 0;

		// Set special buttons visibility if mainframe already exists
		if (mainFrame != null) {
			mainFrame.buttonNextVisible(isAIvsAI());
			mainFrame.buttonHelpVisible(!isAIvsAI());
			mainFrame.buttonHelpEnabled(true);
		}
	}
	
	/**
	 * Places a new piece in the position p if a human is in turn. If the player is
	 * an AI, it generates the position. Also checks for victorys/draws and updates the scoreboard
     *
	 * @param p new position if human, null (or whatever) if AI
	 */
	public WinningCombination move(Position p) {
		if (player1Turn) {
			if (!player1IsHuman)
				p = ((AI) player1).newPiece();

			board.applyMovement(p, player1.getPiece());
		} else {
			if (!player2IsHuman)
				p = ((AI) player2).newPiece();

			board.applyMovement(p, player2.getPiece());
		}
		history.addLast(p);
		
		return checkEndRound();
	}

	private WinningCombination checkEndRound() {
		WinningCombination wc;
		Player currentPlayer = getCurrentPlayer();

		wc = board.checkVictory(currentPlayer.getPiece());
		if (wc == null) {  // If nothing special happened
			player1Turn = !player1Turn;  // Changes the turn
			mainFrame.updateCurrentPlayerLabel();

			// Auto-move if the next player is an AI and NOT in AI vs AI mode
			if (getCurrentPlayer() instanceof AI && !isAIvsAI())
				return move(null);
		} else {  // If victory or draw
			if (wc.isDraw())
				Player.draw();
			else
				currentPlayer.victory();

			round++;
			mainFrame.getScoreBoard().update(true);
			mainFrame.buttonNextVisible(true);
			mainFrame.buttonHelpEnabled(false);

			player1Turn = !player1Turn;  // Changes the turn
		}
		return wc;
	}
}
