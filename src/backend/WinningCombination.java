/*
 * Copyright (C) 2018 Esteban López | gnu_stallman (at) protonmail (dot) ch
 *
 * This file is part of TER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package backend;

public class WinningCombination {
    private Position[] positions;
    private boolean draw;

    /**
     * This constructor must be used in draw case
     */
    public WinningCombination() {
        draw = true;
        positions = null;
    }

    /**
     * This constructor must be used in victory case
     */
    public WinningCombination(Position position1, Position position2, Position position3) {
        draw = false;
        positions = new Position[3];
        positions[0] = position1;
        positions[1] = position2;
        positions[2] = position3;
    }

    public Position[] getPositions() {
        return positions;
    }

    public boolean isDraw() {
        return draw;
    }
}
