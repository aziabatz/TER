/*
 * Copyright (C) 2018 Esteban López | gnu_stallman (at) protonmail (dot) ch
 *
 * This file is part of TER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package backend;

import java.util.Random;

public class AI extends Player {
	private Piece pieceAI, pieceEnemy;
	private Board board;
	private Random randomGenerator;

	private enum Orientation {HORIZONTAL, VERTICAL, DIAGONAL}

	/**
	 * AI constructor specifying it's name and piece
	 * 
	 * @param name
	 * @param piece
	 * @param board
	 */
	public AI(String name, Piece piece, Board board) {
		super(name, piece);
		pieceAI = getPiece();
		pieceEnemy = pieceAI == Piece.X ? Piece.O : Piece.X;
		randomGenerator = new Random();
		this.board = board;
	}

	/**
	 * Phase 1 attack:
	 * If there is one piece and two empty boxes consecutively
	 *
	 * Logical function F = a!b!c + !ab!c + !a!bc
	 * a, b and c are the boxes from a row, a column or a diagonal
	 *
	 * @return a position if matches, null if couldn't place
	 */
	private Position phase1Attack(Orientation orientation) {
		Piece[][] table = board.getMatrix();
		boolean a, b, c;

		if (orientation == Orientation.HORIZONTAL) {
			for (int i = 0; i <= 2; i++) {
				// Check there aren't any enemy pieces in the row
				if (table[i][0] != pieceEnemy && table[i][1] != pieceEnemy && table[i][2] != pieceEnemy) {
					a = table[i][0] == pieceAI;
					b = table[i][1] == pieceAI;
					c = table[i][2] == pieceAI;

					if (a && !b && !c)
						return new Position(i, (randomGenerator.nextInt() % 2 == 0 ? 1 : 2));
					else if (!a && b && !c)
						return new Position(i, (randomGenerator.nextInt() % 2 == 0 ? 0 : 2));
					else if (!a && !b && c)
						return new Position(i, (randomGenerator.nextInt() % 2 == 0 ? 0 : 1));
				}
			}
		} else if (orientation == Orientation.VERTICAL) {
			for (int j = 0; j <= 2; j++) {
				// Check there aren't any enemy pieces in the column
				if (table[0][j] != pieceEnemy && table[1][j] != pieceEnemy && table[2][j] != pieceEnemy) {
					a = table[0][j] == pieceAI;
					b = table[1][j] == pieceAI;
					c = table[2][j] == pieceAI;

					if (a && !b && !c)
						return new Position((randomGenerator.nextInt() % 2 == 0 ? 1 : 2), j);
					else if (!a && b && !c)
						return new Position((randomGenerator.nextInt() % 2 == 0 ? 0 : 2), j);
					else if (!a && !b && c)
						return new Position((randomGenerator.nextInt() % 2 == 0 ? 0 : 1), j);
				}
			}
		} else if (orientation == Orientation.DIAGONAL) {
			// The following four possible elections aren't randomized because taking the center has priority
			if (table[0][0] == pieceAI && table[1][1] == Piece.EMPTY && table[2][2] == Piece.EMPTY)
				return new Position(1, 1);
			else if (table[0][2] == pieceAI && table[1][1] == Piece.EMPTY && table[2][0] == Piece.EMPTY)
				return new Position(1, 1);
			else if (table[2][0] == pieceAI && table[1][1] == Piece.EMPTY && table[0][2] == Piece.EMPTY)
				return new Position(1, 1);
			else if (table[2][2] == pieceAI && table[1][1] == Piece.EMPTY && table[0][0] == Piece.EMPTY)
				return new Position(1, 1);
			else if (table[1][1] == pieceAI) {
				if (table[0][0] == Piece.EMPTY && table[2][2] == Piece.EMPTY)
					if (randomGenerator.nextInt() % 2 == 0)
						return new Position(0, 0);
					else
						return new Position(2, 2);
				else if (table[2][0] == Piece.EMPTY && table[0][2] == Piece.EMPTY)
					if (randomGenerator.nextInt() % 2 == 0)
						return new Position(2, 0);
					else
						return new Position(0, 2);
			}
		}

		return null;  // If no combination matches
	}

	/**
	 * Makes the IA choose a box to place its piece on
	 *
	 * @return the position chosen by the AI to place a piece. Null if it couldn't find any free box
	 */
	public Position newPiece() {
		Piece[][] table = board.getMatrix();
		boolean a, b, c;

		// Initial movement must be a corner or the center
		if (board.countPiecesPlaced() == 0) {
			if (randomGenerator.nextInt() % 2 == 0) {
				return new Position(1, 1);
			} else {
				// corner's coordinates are made by 2 and 0
				return new Position((randomGenerator.nextInt() % 2 == 0 ? 0 : 2), (randomGenerator.nextInt() % 2 == 0 ? 0 : 2));
			}
		}

		// In the second turn the center should be taken, if the enemy have it's piece already there,take a corner
		if (board.countPiecesPlaced() == 1) {
			if (table[1][1] == Piece.EMPTY)
				return new Position(1,1);
			else if (table[1][1] == pieceEnemy)
				return new Position((randomGenerator.nextInt() % 2 == 0 ? 0 : 2), (randomGenerator.nextInt() % 2 == 0 ? 0 : 2));
		}

		/*
		 * Phase 2 attack:
		 * If there are two IA's pieces and a free box consecutively
		 * 
		 * Logical function F = !abc + a!bc + ab!c
		 * a, b and c are the boxes from a row, a column or a diagonal
		 */

		// Horizontal
		for (int i = 0; i <= 2; i++) {				
			// Check there aren't any enemy pieces in the row
			if (table[i][0] != pieceEnemy && table[i][1] != pieceEnemy && table[i][2] != pieceEnemy) {
				a = table[i][0] == pieceAI;
				b = table[i][1] == pieceAI;
				c = table[i][2] == pieceAI;					

				if (!a && b && c)
					return new Position(i, 0);
				else if (a && !b && c)
					return new Position(i, 1);
				else if (a && b && !c)
					return new Position(i, 2);
			}
		}

		// Vertical
		for (int j = 0; j <= 2; j++) {
			// Check there aren't any enemy pieces in the column
			if (table[0][j] != pieceEnemy && table[1][j] != pieceEnemy && table[2][j] != pieceEnemy) {
				a = table[0][j] == pieceAI;
				b = table[1][j] == pieceAI;
				c = table[2][j] == pieceAI;					

				if (!a && b && c)
					return new Position(0, j);
				else if (a && !b && c)
					return new Position(1, j);
				else if (a && b && !c)
					return new Position(2, j);
			}
		}

		// Diagonal
		if (table[0][0] == pieceAI && table[1][1] == pieceAI && table[2][2] == Piece.EMPTY)				
			return new Position(2, 2);
		else if (table[2][2] == pieceAI && table[1][1] == pieceAI && table[0][0] == Piece.EMPTY) 
			return new Position(0, 0);
		else if (table[2][0] == pieceAI && table[1][1] == pieceAI && table[0][2] == Piece.EMPTY)
			return new Position(0, 2);
		else if (table[0][2] == pieceAI && table[1][1] == pieceAI && table[2][0] == Piece.EMPTY)
			return new Position(2,0);
		else if (((table[0][0] == pieceAI && table[2][2] == pieceAI) || (table[2][0] == pieceAI && table[0][2] == pieceAI )) 
				&& table[1][1] == Piece.EMPTY)
			return new Position(1, 1);

		/*
		 * DEFENSE 
		 * If the enemy is in phase 2 attack
		 */

		// Horizontal
		for (int i = 0; i <= 2; i++) {				
			// Check there aren't any own pieces in the row
			if (table[i][0] != pieceAI && table[i][1] != pieceAI && table[i][2] != pieceAI) {
				a = table[i][0] == pieceEnemy;
				b = table[i][1] == pieceEnemy;
				c = table[i][2] == pieceEnemy;					

				if (!a && b && c)
					return new Position(i, 0);
				else if (a && !b && c)
					return new Position(i, 1);
				else if (a && b && !c)
					return new Position(i, 2);
			}
		}
		
		// Vertical
		for (int j = 0; j <= 2; j++) {
			// Check there aren't any own pieces in the column
			if (table[0][j] != pieceAI && table[1][j] != pieceAI && table[2][j] != pieceAI) {
				a = table[0][j] == pieceEnemy;
				b = table[1][j] == pieceEnemy;
				c = table[2][j] == pieceEnemy;					

				if (!a && b && c)
					return new Position(0, j);
				else if (a && !b && c)
					return new Position(1, j);
				else if (a && b && !c)
					return new Position(2, j);
			}
		}

		// Diagonal
		if (table[0][0] == pieceEnemy && table[1][1] == pieceEnemy && table[2][2] == Piece.EMPTY)				
			return new Position(2, 2);
		else if (table[2][2] == pieceEnemy && table[1][1] == pieceEnemy && table[0][0] == Piece.EMPTY) 
			return new Position(0, 0);
		else if (table[2][0] == pieceEnemy && table[1][1] == pieceEnemy && table[0][2] == Piece.EMPTY)
			return new Position(0, 2);
		else if (table[0][2] == pieceEnemy && table[1][1] == pieceEnemy && table[2][0] == Piece.EMPTY)
			return new Position(2,0);
		else if (((table[0][0] == pieceEnemy && table[2][2] == pieceEnemy) || (table[2][0] == pieceEnemy && table[0][2] == pieceEnemy)) 
				&& table[1][1] == Piece.EMPTY)
			return new Position(1, 1);

		/*
		 * Phase 1 attack:
		 * If there is one piece and two empty boxes consecutively 
		 * 
		 * Logical function F = a!b!c + !ab!c + !a!bc
		 * a, b and c are the boxes from a row, a column or a diagonal
		 */

		// Check order must change to avoid select always the same orientation

		int orderSelector = randomGenerator.nextInt() % 3;
		Position position;

		if (orderSelector == 0) {  // First horizontal
			position = phase1Attack(Orientation.HORIZONTAL);
			if (position != null)
				return position;
			position = phase1Attack(Orientation.VERTICAL);
			if (position != null)
				return position;
			position = phase1Attack(Orientation.DIAGONAL);
			if (position != null)
				return position;
		} else if (orderSelector == 1) {  // First vertical
			position = phase1Attack(Orientation.VERTICAL);
			if (position != null)
				return position;
			position = phase1Attack(Orientation.HORIZONTAL);
			if (position != null)
				return position;
			position = phase1Attack(Orientation.DIAGONAL);
			if (position != null)
				return position;
		} else {  // First diagonal
			position = phase1Attack(Orientation.DIAGONAL);
			if (position != null)
				return position;
			position = phase1Attack(Orientation.VERTICAL);
			if (position != null)
				return position;
			position = phase1Attack(Orientation.HORIZONTAL);
			if (position != null)
				return position;
		}
		
		// Last resource
		for (int i = 0; i <= 2; i++)
			for (int j = 0; j <= 2; j++)
				if (table[i][j] == Piece.EMPTY)
					return new Position(i, j);
		
		return null;
	}
}
