/*
 * Copyright (C) 2018 Esteban López | gnu_stallman (at) protonmail (dot) ch
 *
 * This file is part of TER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package backend;

import java.util.Arrays;
import java.util.Collections;

public class Board {
	private Piece[][] board;

	private static Board instance;/**Only one instace of board per execution*/
	
	/**
	 * Board constructor
	 */
	private Board() {
		board = new Piece[3][3];
		cleanBoard();
	}
	
	/**
	 * @return the board matrix
	 */
	public Piece[][] getMatrix() {
		return board;
	}
	
	/**
	 * Initializes the board. Sets all values to EMPTY
	 */
	public void cleanBoard() {
		for (int i = 0; i < board.length; i++) {
			Arrays.fill(board[i], Piece.EMPTY);
		}
	}
	
	/**
	 * Places a new piece on the board
	 * 
	 * @param position the position of the new piece
	 * @param piece the new piece
	 * @return true if the piece was placed
	 */
	public boolean applyMovement(Position position, Piece piece) {
		// Only applies the movement if the box is empty
		if (board[position.getRow()][position.getColumn()] == Piece.EMPTY) {
			board[position.getRow()][position.getColumn()] = piece;
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Checks if there's a winning combination formed by pieces
	 * 
	 * @param piece the piece to check if won
	 * @return a WinningCombination if victory or draw and null if nothing
	 */
	public WinningCombination checkVictory(Piece piece) {
		WinningCombination combination;

		combination = checkDiagonals(piece);
		if (combination != null)
			return combination;

		combination = checkHorizontals(piece);
		if (combination != null)
			return combination;

		combination = checkVerticals(piece);
		if (combination != null)
			return combination;

		// Checks for a draw
		if (checkFull())
			return new WinningCombination();

		return null;
	}
	
	/**
	 * Counts how many pieces are places on the board
	 * 
	 * @return number of pieces
	 */
	public int countPiecesPlaced() {
		int pieces = 0;
		for (int i = 0; i <= 2; i++)
			for (int j = 0; j <= 2; j++)
				if (board[i][j] != Piece.EMPTY)
					pieces++;
		
		return pieces;
	}
	
	/**
	 * Checks if there's a diagonal winning combination formed by pieces
	 * 
	 * @param piece the piece to check if won
	 * @return a WinningCombination if there's a diagonal winning combination. Null if not
	 */
	private WinningCombination checkDiagonals(Piece piece) {
		if (board[1][1] == piece) {
			if(board[0][0] == piece && board[2][2] == piece)
				return new WinningCombination(new Position(0,0), new Position(1,1), new Position(2,2));
			if(board[0][2] == piece && board[2][0] ==piece)
				return new WinningCombination(new Position(0,2), new Position(1,1), new Position(2,0));
		}
		return null;
	}
	
	/**
	 * Checks if there's a horizontal winning combination formed by pieces
	 * 
	 * @param piece the piece to check if won
	 * @return a WinningCombination if there's a horizontal winning combination. Null if not
	 */
	private WinningCombination checkHorizontals(Piece piece) {
		boolean row;
		for (int i = 0; i <= 2; i++) {
			row = true;
			
			for (int j = 0; j <= 2; j++) {
				if (board[i][j] != piece)
					row = false;
			}
			
			if (row)
				return new WinningCombination(new Position(i,0), new Position(i,1), new Position(i,2));
		}
		
		return null;
	}
	
	/**
	 * Checks if there's a vertical winning combination formed by pieces
	 * 
	 * @param piece the piece to check if won
	 * @return a WinningCombination if there's a vertical winning combination. Null if not
	 */
	private WinningCombination checkVerticals(Piece piece) {
		boolean column;
		for (int j = 0; j <= 2; j++) {
			column = true;
			
			for (int i = 0; i <= 2; i++) {
				if (board[i][j] != piece)
					column = false;
			}
			
			if (column)
				return new WinningCombination(new Position(0, j), new Position(1, j), new Position(2, j));
		}
		
		return null;
	}
	
	/**
	 * Checks if there aren't more empty boxes on the board
	 * 
	 * @return true if there aren't more empty boxes on the board
	 */
	private boolean checkFull() {
		return countPiecesPlaced() == 9;
	}

	/**
	 * Returns an instance of the board
	 * 
	 * @return instance
	 */
	public static Board getBoardInstance(){
		if(instance==null)
			instance = new Board();
		return instance;
	}
}
