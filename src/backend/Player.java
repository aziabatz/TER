/*
 * Copyright (C) 2018 Esteban López | gnu_stallman (at) protonmail (dot) ch
 *
 * This file is part of TER
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package backend;

public class Player {
	private String name;
	private int points;
	private static int drawPoints = 0;
	private Piece piece;
	
	/**
	 * Player constructor specifying the name and the piece of the player
	 * 
	 * @param name his name
	 * @param piece his piece
	 */
	public Player(String name, Piece piece) {
		this.name = name;
		this.piece = piece;
		points = 0;
	}
	
	/**
	 * @param points the points to set
	 */
	public void setPoints(int points) {
		this.points = points;
	}

	/**
	 * @param drawPoints
	 */
	public static void setDrawPoints(int drawPoints) {
		Player.drawPoints = drawPoints;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the points
	 */
	public int getPoints() {
		return points;
	}

	/**
	 * @return the drawPoints
	 */
	public static int getDrawPoints() {
		return drawPoints;
	}

	/**
	 * @return the piece
	 */
	public Piece getPiece() {
		return piece;
	}

	/**
	 * Increments the player's punctiation
	 */
	public void victory() {
		points++;
	}
	
	/**
	 * Increments the draw's count
	 */
	public static void draw() {
		Player.drawPoints++;
	}
	
}
